#!/usr/bin/env python
import datetime
import os
import socket
import threading
import time
import traceback

import requests
from sqlalchemy import create_engine, Integer, Unicode, DateTime, Column, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

FETCH_SLEEP = 10
HOST = os.environ.get('HISTORY_HOSTNAME') or socket.gethostname()
BROADCASTER_URL = 'http://pisquared.xyz/{}'
BASH_ETERNAL_HISTORY = os.path.join(os.environ.get('HOME'), '.bash_eternal_history')

Base = declarative_base()
sqllite_path = os.path.join(os.getcwd(), 'local_history.db')
engine = create_engine('sqlite:///{}'.format(sqllite_path), echo=False)

session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)


def create_db():
    Base.metadata.create_all(engine)


def session_commit(session):
    try:
        session.commit()
    except:
        session.rollback()
    finally:
        session.close()


class HistoryCommand(Base):
    __tablename__ = 'history_command'

    id = Column(Integer, primary_key=True)
    host = Column(Unicode)
    created_dt = Column(DateTime)
    command = Column(Unicode)
    synced = Column(Boolean, default=False)

    @staticmethod
    def ts_to_dt(ts):
        return datetime.datetime.fromtimestamp(int(ts))

    @property
    def created_ts(self):
        return self.created_dt.strftime("%s")

    def serialize(self):
        return {
            'id': self.id,
            'command': self.command,
            'created_dt': str(self.created_dt),
            'created_ts': self.created_ts,
            'synced': self.synced,
            'host': self.host,
        }

    def __repr__(self):
        return '<HistoryCommand (%r): %r>' % (self.created_dt, self.command)


def batch_post_request(history_commands):
    session = Session()
    for history_command in history_commands:
        session.add(history_command)
    print("  > Posting: {}".format(len(history_commands)))
    try:
        response = requests.post(BROADCASTER_URL.format("new_batch"),
                                 json=[history_command.serialize() for history_command in history_commands])
        print("  > Success!")
        response.json()
        for history_command in history_commands:
            history_command.synced = True
            session.add(history_command)
    except:
        print("  > FAIL!")
        traceback.print_exc()
    session_commit(session)


def post_request(history_command):
    session = Session()
    session.add(history_command)
    print("  > Posting: {}".format(history_command))
    try:
        response = requests.post(BROADCASTER_URL.format("new"),
                                 data={
                                     'ts': history_command.created_ts,
                                     'command': history_command.command,
                                     'host': HOST
                                 })
        print("  > Success!")
        response.json()
        history_command.synced = True
    except:
        print("  > FAIL!")
        traceback.print_exc()
        history_command.synced = False
    session.add(history_command)
    session_commit(session)


def follow(fname):
    with open(BASH_ETERNAL_HISTORY) as f:
        f.seek(0, 2)  # Go to the end of the file
        # HACK: in order not to hang on the last command
        yield_once = False
        while True:
            line = f.readline()
            if not line:
                if yield_once:
                    yield_once = False
                    yield
                time.sleep(0.1)  # Sleep briefly
                continue
            yield line
            print(">>> Listening for commands...")
            yield_once = True


def parse_history_file(iterable):
    create, ts, command = False, None, ""
    for line in iterable:
        print(">>> parsing {}".format(line))
        if line is None or (line.startswith('#1') and len(line) == 12):
            if not ts:
                ts = int(line[1:]) if line.startswith('#') else int(line)
            else:
                history_command = HistoryCommand(command=command,
                                                 created_dt=HistoryCommand.ts_to_dt(ts),
                                                 host=HOST)
                print(">>> Created {}".format(history_command))
                if line is None:
                    ts, command = 0, ""
                else:
                    ts, command = int(line[1:]) if line.startswith('#') else int(line), ""
                yield history_command
        else:
            command += line


def get_local_all():
    with open(BASH_ETERNAL_HISTORY) as f:
        local_commands = parse_history_file(iterable=f.readlines())
    return [c for c in local_commands]


def fetch_remote_all():
    remote_commands = []
    try:
        response = requests.get(BROADCASTER_URL.format('get'))
    except:
        print('No connection to server!')
        return remote_commands
    for command_json in response.json():
        history_command = HistoryCommand(command=command_json['command'],
                                         created_dt=HistoryCommand.ts_to_dt(command_json['created_ts']),
                                         host=command_json['host'],
                                         )
        remote_commands.append(history_command)
    return remote_commands


def create_commands_cache(commands):
    commands_cache = {}
    for command in commands:
        commands_cache[command.created_ts] = command
    return commands_cache


def merge_local_remote(local_commands, remote_commands):
    push_session = []
    session = Session()
    local_commands_c = create_commands_cache(local_commands)
    remote_commands_c = create_commands_cache(remote_commands)
    for local_command in local_commands:
        session.add(local_command)
        if local_command not in remote_commands_c:
            # local command not in remote, add it to be pushed
            push_session.append(local_command)
    for remote_command in remote_commands:
        if remote_command.created_ts not in local_commands_c:
            # remote command is not local, add it and mark as synced
            remote_command.synced = True
            session.add(remote_command)
    session_commit(session)
    # push local to remote srv
    batch_post_request(push_session)


def get_local_unsynced():
    session = Session()
    unsynced_history_commands = session.query(HistoryCommand).filter_by(synced=False).all()
    if unsynced_history_commands:
        print(">>> {} unsynced commands".format(len(unsynced_history_commands)))
    return unsynced_history_commands


def fetch_remote_unsynced():
    while True:
        session = Session()
        try:
            last_not_host = session.query(HistoryCommand).filter(HistoryCommand.host != HOST).order_by(
                HistoryCommand.created_dt.desc()).first()
            if last_not_host:
                since = last_not_host.created_ts
            else:
                since = 0
            response = requests.get(BROADCASTER_URL.format('get'),
                                    params={
                                        'since': since,
                                        'host_not': HOST
                                    })
            remote_commands = response.json()
            for remote_command in remote_commands:
                history_command = HistoryCommand(command=remote_command['command'],
                                                 created_dt=HistoryCommand.ts_to_dt(remote_command['created_ts']),
                                                 host=remote_command['host'],
                                                 synced=True)
                session.add(history_command)
            if remote_commands:
                session_commit(session)
                print("  > Created {} fetched elements!".format(len(remote_commands)))
        except:
            print("  > Failed to fetch unsynced!")
        time.sleep(FETCH_SLEEP)


def parse_push():
    for history_command in parse_history_file(iterable=follow(BASH_ETERNAL_HISTORY)):
        post_request(history_command)


if __name__ == '__main__':
    if not os.path.exists(sqllite_path):
        # The local database doesn't exist, initialize by creating it
        create_db()
        # Get local commands from history file
        local_commands = get_local_all()
        # Fetch remote commands and then merge the two
        remote_commands = fetch_remote_all()
        merge_local_remote(local_commands, remote_commands)
    # In any case, push the local unsynced to the server
    unsynced_commands = get_local_unsynced()
    batch_post_request(unsynced_commands)
    # Start fetching remote commands in a thread
    fetch_thread = threading.Thread(target=fetch_remote_unsynced)
    try:
        fetch_thread.start()
        # Start monitoring the local history file
        parse_push()
    except KeyboardInterrupt:
        print("Caught Keyboard Interrupt, quitting...")
        fetch_thread.join()
